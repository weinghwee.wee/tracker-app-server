const express = require('express');

const requestHandler = require('./requestHandler');

const app = express();

const route = (requestMethod, routeName, method) => app[requestMethod](routeName, requestHandler(method));

module.exports = [app, route];
