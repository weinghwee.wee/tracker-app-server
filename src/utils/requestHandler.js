const auth = require('../auth/auth');

const createResponse = (req, res) => {
  const { result, error } = req;
  if (req.error) {
    return res.status(400).send({
      error,
    });
  }

  res.status(200).send({
    result,
  });
};

module.exports = (method) => {
  const methodHandler = async (req, res, next) => {
    if (req.error) {
      return next();
    }

    await method(req, res);

    next();
  };

  return [
    auth,
    methodHandler,
    createResponse,
  ];
};
