const Track = require('./models/Track');

module.exports.getTracks = async (userId) => {
  const tracks = await Track.find({ userId });

  return tracks;
};

module.exports.saveTrack = async (userId, name, locations) => {
  try {
    const track = new Track({ name, locations, userId });
    await track.save();

    return track;
  } catch (err) {
    return err.message;
  }
};
