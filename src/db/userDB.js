const jwt = require('jsonwebtoken');

const User = require('./models/User');

module.exports.createUser = async (email, password) => {
  try {
    const user = new User({ email, password });
    await user.save();

    const token = jwt.sign({
      userId: user._id
    }, 'MY_SECRET_KEY');

    return { token };
  } catch (err) {
    return err.message;
  }
};

module.exports.findUser = async (email) => {
  const user = await User.findOne({ email });

  if (!user) {
    return 'Invalid email or password';
  }

  return user;
};
