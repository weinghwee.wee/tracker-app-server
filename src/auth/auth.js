const jwt = require('jsonwebtoken');

const User = require('../db/models/User');

module.exports = (req, res, next) => {
  const { authorization } = req.headers;

  if (!authorization) {
    req.error = 'Authorization failed';

    return next();
  }

  const token = authorization.replace('Bearer ', '');

  jwt.verify(token, process.env.jwtSecret, async (err, payload) => {
    if (err) {
      req.error = 'Authorization failed';

      return next();
    }

    const { userId } = payload;

    const user = await User.findById(userId);

    req.user = user;
    next();
  });
};
