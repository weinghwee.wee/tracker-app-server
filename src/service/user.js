const jwt = require('jsonwebtoken');
const db = require('../db');

const { user } = db;


module.exports.signUp = async (req, res) => {
  const { email, password } = req.body;
  const result = await user.createUser(email, password);

  if (result.token) {
    return res.send({ token: result.token });
  }

  res.status(400).send({ error: result });
};

module.exports.signIn = async (req, res) => {
  const { email, password } = req.body;

  const result = await user.findUser(email);

  if (!result.email) {
    return res.status(400).send({ error: result });
  }

  try {
    await result.comparePassword(password);
    const token = jwt.sign({ userId: result._id }, process.env.jwtSecret);

    res.send({ token });
  } catch (err) {
    res.status(400).send({ error: 'Invalid password or email' });
  }
};
