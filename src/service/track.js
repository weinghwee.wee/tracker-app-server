const db = require('../db');

const { track } = db;

module.exports.getTracks = async (req, res) => {
  const result = await track.getTracks(req.user.userId);

  req.result = result;
};

module.exports.saveTrack = async (req, res) => {
  const { name, location } = req.body;
  const result = await track.saveTrack(req.user.userId, name, location);

  if (result._id) {
    req.result = result;

    return;
  }

  req.error = result;
};
