const bodyParser = require('body-parser');
const [app, route] = require('./src/utils/route');
const service = require('./src/service');

require('dotenv').config({ path: './config/.env' });

app.use(bodyParser.json());

require('./src/db/mongoose');

// user
const { user } = service;

app.post('/signin', (req, res) => { user.signIn(req, res); });
app.post('/signup', (req, res) => { user.signUp(req, res); });

// track
const { track } = service;

route('get', '/tracks', track.getTracks);
route('post', '/saveTracks', track.saveTrack);

app.listen(3000, () => {
  console.log('Connected to port 3000');
});
